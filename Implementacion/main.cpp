#include <iostream>
#include <fstream>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include "TFloat.h"
#include "TFloat.cpp"
using namespace std;

TFloat pow(TFloat base, int pot){ // para potencias naturales: [1 .. inf)
    TFloat acum (1,base.precision());
    if (pot >= 1){
        for (int i = 0 ; i < pot ; i++){
            acum = acum * base;
        }
    }
    return acum;
}

TFloat M(TFloat datos[], int n, TFloat s){
    TFloat acum (0,datos[0].precision());
    for (int i=0 ; i<n ; i++){
        acum = acum + /*Temporal -->*/TFloat(pow(datos[i].dbl(),s.dbl()), datos[i].precision()); //pow(datos[i],s);
    }
    acum = acum / n;
    return acum;
}

TFloat Mprimera(TFloat datos[], int n, TFloat s){ //derivada primera de M
    TFloat acum (0,datos[0].precision());
    for (int i=0 ; i<n ; i++){
        acum = acum + /*Temporal -->*/ TFloat(pow(datos[i].dbl(),s.dbl()), datos[i].precision()) * TFloat(log(datos[i].dbl()), datos[i].precision()); //(pow(datos[i],s))*(log(datos[i]));
    }
    acum = acum / n;

    return acum;
}

TFloat Msegunda(TFloat datos[], int n, TFloat s){ // derivada segunda de M
    TFloat acum (0,datos[0].precision());
    for (int i=0 ; i<n ; i++){
        acum = acum + /*Temporal -->*/ TFloat(pow(datos[i].dbl(),s.dbl()), datos[i].precision()) * pow(TFloat(log(datos[i].dbl()),datos[i].precision()),2); //(pow(datos[i],s))*(pow(log(datos[i]),2));
    }
    acum = acum / n;
    return acum;
}

TFloat R(TFloat datos[], int n, TFloat s){
    return Mprimera(datos,n,s) / M(datos,n,s);
}

TFloat Rprimera(TFloat datos[], int n, TFloat s){ // derivada primera de R
    return ((Msegunda(datos,n,s)*M(datos,n,s) - pow(Mprimera(datos,n,s),2)) / (pow(M(datos,n,s),2)));
}

TFloat funcion(TFloat datos[], int n, TFloat beta){ // funcion principal
    TFloat dos (2,datos[0].precision());
    TFloat uno (1,datos[0].precision());
    TFloat acum = M(datos,n,dos*beta) / (pow(M(datos,n,beta),2));
    acum = acum - (beta*(R(datos,n,beta)-R(datos,n,0)) + uno);
    return acum;
}

TFloat derivada(TFloat datos[], int n, TFloat beta){ // derivada de funcion()
    TFloat dos (2,datos[0].precision());
    TFloat acum = Mprimera(datos,n,dos*beta)*dos*(pow(M(datos,n,beta),2)) - M(datos,n,dos*beta)*dos*Mprimera(datos,n,beta);
    acum = acum / pow(M(datos,n,beta),4);
    acum = acum - (R(datos,n,beta) + beta*Rprimera(datos,n,beta) - R(datos,n,0));
    return acum;
}

TFloat lambda(TFloat datos[], int n, TFloat beta){
       TFloat cero (0,datos[0].precision());
       TFloat uno (1,datos[0].precision());
       TFloat acum = R(datos,n,beta) - R(datos,n,cero);
       acum = beta * acum;
       acum = uno / acum;
       return acum;
}

TFloat sigma (TFloat datos[], int n, TFloat beta, TFloat lambda){
    TFloat acum = M(datos,n,beta) / lambda;
    TFloat exp (1/(beta.dbl()),datos[0].precision());
    acum = TFloat (pow(acum.dbl(),exp.dbl()), acum.precision());
    return acum;
}


/// Metodo de Newton.

TFloat Newton(TFloat datos[], int n, TFloat x0, int iteraciones, double epsilon){
    int i = 0;
    TFloat xn = x0;
    TFloat x_anterior;
    do{
        x_anterior = xn;
        xn = xn - (funcion(datos, n, xn)/derivada(datos, n, xn));
        i++;
    }while(i<iteraciones && epsilon<fabs((xn-x_anterior).dbl()));

    cout << "Me paro: " << (i>=iteraciones? "Iteraciones" : "Epsilon") << ", en iteracion: " << i << endl;
    return xn;
}

/// Secante


TFloat Secante(TFloat datos[], int n, TFloat x0, TFloat x1, int iteraciones, double epsilon){
    TFloat xn = x1;
    TFloat x_anterior = x0;
    TFloat acum;
    int i;
    for (i = 0 ; (i<iteraciones && epsilon<fabs((xn-x_anterior).dbl())) ; i++){
        acum = (xn - x_anterior) / (funcion(datos, n, xn) - funcion(datos, n, x_anterior));
        x_anterior = xn;
        xn = xn - (funcion(datos, n, xn) * acum);
    }

    cout << "Me paro: " << (i>=iteraciones? "Iteraciones" : "Epsilon") << ", en iteracion: " << i << endl;
    return xn;
}

void test_2(){
	
    ifstream archivoin;
    ofstream archivout1, archivout2, archivout3;
    ///Datos de Entrada
    TFloat pto_sugerido1,pto_sugerido2, aproximacion;
    int n,i = 0;
    TFloat resultado;
    double aux1=15, aux2=12, aux3=14.9, dato, epsilon = 0.001;
    
    ///Archivo de entrada
    archivoin.open("x_2_62_35.txt");
    
    ///Archivos de Salida
    archivout1.open("test2_Newton.txt");
    archivout2.open("Test2_Secante_primero.txt");
    archivout3.open("test2_Secante_segundo.txt");

    archivoin >> n;
    
    TFloat datos[n];
    
    while (!archivoin.eof()){
        archivoin >> dato;
        datos[i] = TFloat(dato,52);
        i++;
    }

    pto_sugerido1 = TFloat(aux1,i);
    pto_sugerido2 = TFloat(aux2,i);

    ///Mido Newton
    i = 0;
    archivout1 << 0 << "\t" << pto_sugerido1.dbl() << endl;
    
    cout << endl << endl;
    
    while(i < 800){
		i += 5;
		resultado = Newton(datos,n,pto_sugerido1,i,epsilon);
		
		///Guardo aproximacion
		archivout1 << i << "\t" << resultado.dbl() << endl;
	
	}

    ///Mido Secante 1
    i = 0;
    archivout2 << 0 << "\t" << pto_sugerido1.dbl() << endl;
    archivout2 << 0 << "\t" << pto_sugerido2.dbl() << endl;
    
    cout << endl << endl;
    
    while(i < 10){
		i++;
		resultado = Secante(datos,n,pto_sugerido1,pto_sugerido2,i,epsilon);
		
		/// Guardo aproximacion
		archivout2 << i << "\t" << resultado.dbl() << endl;
	}
	
	///Mido Secante 2
    i = 0;
    pto_sugerido2 = TFloat(aux3,i);
    archivout3 << 0 << "\t" << pto_sugerido1.dbl() << endl;
    archivout3 << 0 << "\t" << pto_sugerido2.dbl() << endl;
    
    cout << endl << endl;
    
    while(i < 12){
		i++;
		resultado = Secante(datos,n,pto_sugerido1,pto_sugerido2,i,epsilon);
		
		/// Guardo aproximacion
		archivout3 << i << "\t" << resultado.dbl() << endl;
	}

    archivoin.close();
    archivout1.close();
    archivout2.close();
    archivout3.close();
}

void test_3(){
	
    ifstream archivoin;
    ofstream archivout1,archivout2;
    clock_t comienzo, fin, tiempo;
    ///Datos de Entrada
    TFloat pto_sugerido1,pto_sugerido2, aproximacion;
    int n,i = 0;
    float resultado;
    double aux1=15, aux2=12, epsilon = 0.00000000000001;
    
    ///Archivo de entrada
    archivoin.open("x_2_62_35.txt");
    
    ///Archivos de Salida
    archivout1.open("test3_Newton.txt");
    archivout2.open("test3_Secante.txt");

    archivoin >> n;
    double datos[n];
    TFloat datos2[n];
    while (!archivoin.eof()){
        archivoin >> datos[i];
        i++;
    }
    ///Itero para distintas precisiones
    for ( i = 50 ; i>=15 ; i-=5){
        cout << "Test con precision: " << i << endl;
        for (int j = 0 ; j < n ; j++){
			
            /// Cargo datos con la precision i
            datos2[j] = TFloat(datos[j],i);
            
        }

        pto_sugerido1 = TFloat(aux1,i);
        pto_sugerido2 = TFloat(aux2,i);

        ///Mido tiempo newton
        comienzo = clock();
        Newton(datos2,n,pto_sugerido1,50,epsilon);
        fin = clock();
        tiempo = difftime(fin,comienzo);
        resultado = ((float)tiempo)/CLOCKS_PER_SEC;
        
        ///Guardo tiempo medido
        archivout1 << i << "\t" << resultado << endl;

        ///Mido tiempo Secante
        comienzo = clock();
        Secante(datos2,n,pto_sugerido1,pto_sugerido2,10,epsilon);
        fin = clock();
        tiempo = difftime(fin,comienzo);
        resultado = ((float)tiempo)/CLOCKS_PER_SEC;

        /// Guardo tiempo medido
        archivout2 << i << "\t" << resultado << endl;
    }

    archivoin.close();
    archivout1.close();
    archivout2.close();
}

int main()
{
	char* nombre;
    clock_t comienzo, fin, tiempo;
    ifstream archivo;
    int decision, n, precision, iteraciones, i=0;
    double aux, aux1, aux2, epsilon;
    TFloat pto_sugerido1,pto_sugerido2;
    TFloat beta_aprox, lambda_aprox, sigma_aprox;

    /// Leo los datos de un archivo.

    cout << "Archivo a Abrir: ";
    cin >> nombre;
    cout << endl;
    archivo.open(nombre);
    while (!archivo.good()){
          cout << "El archivo no existe. Ingrese otro: ";
          cin >> nombre;
          cout << endl;
          archivo.open(nombre);
    }
    archivo >> n;
    TFloat datos[n];

    /// Eleccion del metodo a utilizar

    cout << "Seleccionar Metodo: " << endl;
    cout << "1) Newton " << endl;
    cout << "2) Secante " << endl;
    cin >> decision;

    /// Entrada de Especificaciones:

    cout << "Precision: ";
    cin >> precision;
    cout << endl;
    cout << "Cantidad de iteraciones: ";
    cin >> iteraciones;
    cout << endl;
    cout << "Epsilon: ";
    cin >> epsilon;
    cout << endl;
    cout << "Punto sugerido inicial: ";
    cin >> aux1;
    cout << endl;

    /// Procesamiento de los datos segun lo especificado:

    while (!archivo.eof()){
        archivo >> aux;
        datos[i] = TFloat(aux,precision);
        i++;
    }

    if (decision == 1){ ///Newton

        pto_sugerido1 = TFloat(aux1,precision);

        cout << "Punto sugerido: " << pto_sugerido1.dbl() << endl;
        if (derivada(datos,n,pto_sugerido1)==0){
           cout << "El punto elegido es un maximo/minimo de f(), no puedo aplicar Newton." << endl;
        }else{
           comienzo=clock();
            beta_aprox = Newton(datos,n,pto_sugerido1,iteraciones,epsilon);

        }

    } else if(decision == 2) {  /// Secante

        cout << "2do Punto sugerido inicial: ";
        cin >> aux2;

        if (aux1!=aux2) {

            pto_sugerido1 = TFloat(aux1,precision);
            pto_sugerido2 = TFloat(aux2,precision);


            cout << "1er Punto sugerido: " << pto_sugerido1.dbl() << endl;
            cout << "2do Punto sugerido: " << pto_sugerido2.dbl() << endl;
            comienzo=clock();
            beta_aprox = Secante(datos,n,pto_sugerido1,pto_sugerido2,iteraciones,epsilon);

        }   else {
            cout << "Los puntos elegidos son iguales. El programa no puede continuar." << endl;
        }
        }   else {
            cout << "Eleccion incorrecta." << endl;

    }

	 fin=clock();
    lambda_aprox = lambda(datos,n,beta_aprox);
    sigma_aprox = sigma(datos,n,beta_aprox,lambda_aprox);

    cout << "Beta: " << beta_aprox.dbl() << endl;
    cout << "Lambda: " << lambda_aprox.dbl() << endl;
    cout << "Sigma: " << sigma_aprox.dbl() << endl;
   tiempo=difftime(fin,comienzo);

    printf("Tiempo de ejecucion: %.3f\n" ,(float)tiempo/CLOCKS_PER_SEC);



    return 0;
}
