\select@language {spanish}
\contentsline {section}{\numberline {1}Introducci\'on}{3}
\contentsline {section}{\numberline {2}Desarrollo}{4}
\contentsline {subsection}{\numberline {2.1}Partiendo del Enunciado}{4}
\contentsline {subsection}{\numberline {2.2}Criterio de Parada}{4}
\contentsline {subsection}{\numberline {2.3}Precisi\'on}{4}
\contentsline {subsection}{\numberline {2.4}Consideraciones sobre TFloat}{5}
\contentsline {subsection}{\numberline {2.5}Algoritmos}{5}
\contentsline {subsection}{\numberline {2.6}Consideraciones para los Tests}{5}
\contentsline {section}{\numberline {3}Resultados}{7}
\contentsline {subsection}{\numberline {3.1}Test 1 : Influencia de la precisi\'on en la aproximaci\'on de $\beta $}{7}
\contentsline {subsection}{\numberline {3.2}Test 2: Convergencia, Secante vs Newton}{9}
\contentsline {subsection}{\numberline {3.3}Test 3 :Influencia de la precisi\'on en el tiempo de ejecuci\'on.}{11}
\contentsline {subsection}{\numberline {3.4}Test 4: Elecci\'on de puntos iniciales}{12}
\contentsline {section}{\numberline {4}Discusi\'on}{14}
\contentsline {subsection}{\numberline {4.1}An\'alisis del Test 1}{14}
\contentsline {subsection}{\numberline {4.2}An\'alisis del Test 2}{14}
\contentsline {subsection}{\numberline {4.3}An\'alisis del Test 3}{14}
\contentsline {subsection}{\numberline {4.4}An\'alisis del Test 4}{14}
\contentsline {section}{\numberline {5}Conclusiones}{15}
\contentsline {section}{\numberline {6}Ap\'endices}{16}
\contentsline {subsection}{\numberline {6.1}Ap\'endice A}{16}
\contentsline {subsection}{\numberline {6.2}Ap\'endice B}{18}
