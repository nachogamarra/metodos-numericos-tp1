
\subsection{Ap\'endice A}

%-------------------------------------------------------------------------
{\bf Introducci\'on}

Numerosas aplicaciones utilizan la distribuci\'on Gamma Generalizada (D$\Gamma$G) para modelar datos, cuya funci\'on de densidad se expresa como $$f_\Theta(x)=\frac{\beta\, x^{\beta \lambda - 1}}{\sigma^{\beta \lambda}\, \Gamma(\lambda)} \exp\left\{-\left(\frac{x}{\sigma} \right)^\beta \right\}\qquad\textrm{con } x\in\real_{>0}$$ donde 
$\Gamma(\cdot)$ es la funci\'on Gamma\footnote{La funci\'on Gamma, en el caso entero, es equivalente a la funci\'on factorial: $\Gamma(n)=n!$, $\forall n\in\nat$ } definida como $\Gamma(z)=\int_0^{\infty}{t^{z-1}e^{-t}\,dt}$, y $\Theta$ representa a la tupla de par\'ametros $\Theta=(\sigma,\beta,\lambda)$. El primero se encuentra relacionado con la escala de la funci\'on $f$ y los otros dos con la forma; todos son positivos.
La D$\Gamma$G engloba un conjunto amplio de distribuciones param\'etricas, donde la distribuci\'on exponencial, Weibull o Gamma son casos especiales de \'esta.
En vez de utilizar (y almacenar) el histograma emp\'irico de los datos, representarlos solamente con los par\'ametros de esta distribuci\'on resulta ser, en muchos casos, una opci\'on m\'as que conveniente. El problema que surge consiste en estimar de forma certera y eficiente los par\'ametros de la D$\Gamma$G que mejor ajusta a los datos.



{\bf Estimaci\'on de par\'ametros}


Sean $n$ datos reales positivos $x_1,\dots,x_n$. Estos datos pueden provenir de mediciones de un sat\'elite, p\'ixeles de im\'agenes, etc., y de los cuales asumimos que siguen una distribuci\'on D$\Gamma$G cuyos par\'ametros queremos estimar. Como se vi\'o en la materia \emph{Proba}, existen m\'etodos como `\textsl{estimadores de m\'axima verosimilitud'}, donde a partir de las muestras se pueden estimar par\'ametros de distribuciones. Utilizando este m\'etodo, llegamos a que se deben cumplir las siguientes condiciones:
\begin{eqnarray}
\tilde{\sigma} & = & \left(\frac{\sum_{i=1}^n{x_i^{\tilde{\beta}}}}{n \tilde{\lambda}} \right)^{1/\tilde{\beta}} \label{eqn:sigma}\\
\tilde{\lambda} & = &\left[ \tilde{\beta} \left( \frac{\sum_{i=1}^n{x_i^{\tilde{\beta}} \log x_i}}{\sum_{i=1}^n{x_i^{\tilde{\beta}}}} - \frac{\sum_{i=1}^n{\log x_i}}{n} \right)\right]^{-1} \label{eqn:lambda}\\
 0 & = & \frac{\tilde{\beta}}{n} \sum_{i=1}^n{\log x_i} - \log {\sum_{i=1}^n{x_i^{\tilde{\beta}}}} + \log(n \tilde{\lambda}) - \psi(\tilde{\lambda})
\end{eqnarray}
donde $\tilde{\Theta}=(\tilde{\sigma},\tilde{\beta},\tilde{\lambda})$ son las estimaciones a partir de las muestras de datos y $\psi(\cdot)$ es la `conocida' funci\'on digamma\footnote{$\psi(z) = \frac{\partial}{\partial z} \ln \Gamma (z) = \frac{\Gamma'(z)}{\Gamma(z)}$.} que se define como la derivada logar\'itmica de la funci\'on Gamma. Notar que una una vez estimado el par\'ametro $\beta$, los otros dos par\'ametros pueden obtenerse de las primeras dos ecuaciones. A partir del c\'alculo de los \textsl{momentos} de una variable aleatoria con funci\'on de densidad $f_\Theta(x)$ se pueden obtener las siguientes dos ecuaciones:
\begin{eqnarray}
\log(\eme(2\beta)) -2\log(\eme(\beta)) & = & \log(1+\beta(\ere(\beta)-\ere(0))) \label{eqn:sol1}\\
\frac{\eme(2\beta)}{\eme^2(\beta)} & = & 1+\beta(\ere(\beta)-\ere(0))) \label{eqn:sol2}
\end{eqnarray}
donde 
$$\eme(s) =  \frac{1}{n}\sum_{i=1}^n{x_i^s}\,, \qquad \emeh(s) = \frac{1}{n}\sum_{i=1}^n{x_i^s \log(x_i)}\,,  \qquad \ere(s) = \frac{\emeh(s)}{\eme(s)}$$
Estas ecuaciones tienen como ventaja que no dependen de los otros par\'ametros, utiliz\'andose funciones m\'as sencillas que s\'olo dependen de los datos. Hallando la soluci\'on de cualquiera de las ecuaciones (4) o (5), es posible estimar el par\'ametro $\beta$, y despejar el resto de los par\'ametros utilizando las ecuaciones (1) y (2).

{\bf Enunciado}

El objetivo del trabajo pr\'actico es implementar un programa que permita estimar los par\'ametros $\Theta=(\sigma,\beta,\lambda)$ a partir de un conjunto de $n$ datos. Para ello, se deber\'a resolver la ecuaciones (4) o (5).
Evaluando los distintos m\'etodos vistos en clase que permitan resolver este problema, se deber\'a realizar una implementaci\'on cumpliendo lo siguiente:
\begin{enumerate} 
\item Implementar al menos dos m\'etodos (de los cuales uno de ellos debe ser el m\'etodo de Newton) con aritm\'etica binaria de punto flotante con $t$ d\'igitos de precisi\'on en la mantisa. El valor $t$ debe ser un par\'ametro de la implementaci\'on, con $t<52$.
\item  Realizar experimentos num\'ericos con cada m\'etodo implementado en el \'item anterior elegiendo varias instancias de prueba y en funci\'on de las cantidad de d\'igitos $t$ de precisi\'on en la mantisa (experimentar con al menos 3 valores distintos de $t$).
\item Para cada m\'etodo implementado se deber\'an mostrar resultados obtenidos en cuanto a cantidad necesaria de iteraciones, tiempo de ejecuci\'on, precisi\'on en el resultado, y cualquier otro par\'ametro que considere de inter\'es evaluar.
\item Realizar el gr\'afico del histograma de los datos y el ajuste obtenido. Extraer conclusiones sobre la efectividad de cada m\'etodo observando los resultados anteriores.  
\end{enumerate}


{\bf Formato de archivos de entrada}

El programa debe tomar los datos desde un archivo de texto con el siguiente formato:

\begin{tabular}{|l|} \hline 
\verb|input_file.txt|\\ \hline
\verb|n|\\ 
$\mathtt{x_1\ x_2\ \ldots\ x_n}$ \\ \hline
\end{tabular}

El archivo contiene en la primer l\'inea la cantidad de datos, y en la l\'inea siguiente se encuentran los $n$ datos (reales positivos) separados por espacio. En la web de la materia se publicar\'an varios archivos de prueba para realizar los primeros experimentos.

\vskip 0.5 cm
\hrule
\vskip 0.1 cm



%----------------------------------------------------------------------------

\newpage
\subsection{Ap\'endice B}

A continuaci\'on se presentan el c\'odigo fuente de las funciones utilizadas para resolver las ecuaciones 
que se tratan en este informe.
\newline
\textbf{Potenciaci\'on para tipos de datos TFloat}

\lstset{language=C, breaklines=true, basicstyle=\footnotesize}
\begin{lstlisting}[frame=single]

TFloat pow(TFloat base, int pot){ // para potencias naturales: [1 .. inf)
    TFloat acum (1,base.precision());
    if (pot >= 1){
        for (int i = 0 ; i < pot ; i++){
            acum = acum * base;
        }
    }
    return acum;
}
\end{lstlisting}


\vspace{20pt}
\textbf{Funci\'on $\eme$}

\lstset{language=C, breaklines=true, basicstyle=\footnotesize}
\begin{lstlisting}[frame=single]

TFloat M(TFloat datos[], int n, TFloat s){
    TFloat acum (0,datos[0].precision());
    for (int i=0 ; i<n ; i++){
        acum = acum + /*Temporal -->*/TFloat(pow(datos[i].dbl(),s.dbl()), datos[i].precision()); //pow(datos[i],s);
    }
    acum = acum / n;
    return acum;
}

\end{lstlisting}

\vspace{20pt}
\textbf{Derivada primera de la funci\'on $\eme$ ($\emeh$)}

\lstset{language=C, breaklines=true, basicstyle=\footnotesize}
\begin{lstlisting}[frame=single]

TFloat Mprimera(TFloat datos[], int n, TFloat s){ //derivada primera de M
    TFloat acum (0,datos[0].precision());
    for (int i=0 ; i<n ; i++){
        acum = acum + /*Temporal -->*/ TFloat(pow(datos[i].dbl(),s.dbl()), datos[i].precision()) * TFloat(log(datos[i].dbl()), datos[i].precision()); //(pow(datos[i],s))*(log(datos[i]));
    }
    acum = acum / n;

    return acum;
}

\end{lstlisting}

\vspace{20pt}
\textbf{Derivada segunda de la funci\'on $\eme$}

\lstset{language=C, breaklines=true, basicstyle=\footnotesize}
\begin{lstlisting}[frame=single]

TFloat Msegunda(TFloat datos[], int n, TFloat s){ // derivada segunda de M
    TFloat acum (0,datos[0].precision());
    for (int i=0 ; i<n ; i++){
        acum = acum + /*Temporal -->*/ TFloat(pow(datos[i].dbl(),s.dbl()), datos[i].precision()) * pow(TFloat(log(datos[i].dbl()),datos[i].precision()),2); //(pow(datos[i],s))*(pow(log(datos[i]),2));
    }
    acum = acum / n;
    return acum;
}
\end{lstlisting}

\vspace{20pt}
\textbf{Funci\'on $\ere$}

\lstset{language=C, breaklines=true, basicstyle=\footnotesize}
\begin{lstlisting}[frame=single]

TFloat R(TFloat datos[], int n, TFloat s){
    return Mprimera(datos,n,s) / M(datos,n,s);
}
\end{lstlisting}


\vspace{20pt}
\textbf{Derivada primera de la funci\'on $\ere$}

\lstset{language=C, breaklines=true, basicstyle=\footnotesize}
\begin{lstlisting}[frame=single]
TFloat Rprimera(TFloat datos[], int n, TFloat s){ // derivada primera de R
    return ((Msegunda(datos,n,s)*M(datos,n,s) - pow(Mprimera(datos,n,s),2)) / (pow(M(datos,n,s),2)));
}


\end{lstlisting}


\vspace{20pt}
\textbf{Funci\'on principal de la estimaci\'on de $\beta$ ($f$)}

\lstset{language=C, breaklines=true, basicstyle=\footnotesize}
\begin{lstlisting}[frame=single]

TFloat funcion(TFloat datos[], int n, TFloat beta){ // funcion principal
    TFloat dos (2,datos[0].precision());
    TFloat uno (1,datos[0].precision());
    TFloat acum = M(datos,n,dos*beta) / (pow(M(datos,n,beta),2));
    acum = acum - (beta*(R(datos,n,beta)-R(datos,n,0)) + uno);
    return acum;
}
\end{lstlisting}

\vspace{20pt}
\textbf{Derivada primera de la funci\'on principal ($f'$)}

\lstset{language=C, breaklines=true, basicstyle=\footnotesize}
\begin{lstlisting}[frame=single]
TFloat derivada(TFloat datos[], int n, TFloat beta){ // derivada de funcion()
    TFloat dos (2,datos[0].precision());
    TFloat acum = Mprimera(datos,n,dos*beta)*dos*(pow(M(datos,n,beta),2)) - M(datos,n,dos*beta)*dos*Mprimera(datos,n,beta);
    acum = acum / pow(M(datos,n,beta),4);
    acum = acum - (R(datos,n,beta) + beta*Rprimera(datos,n,beta) - R(datos,n,0));
    return acum;
}

\end{lstlisting}

\vspace{20pt}
\textbf{Ecuaci\'on para la estimaci\'on de $\lambda$}

\lstset{language=C, breaklines=true, basicstyle=\footnotesize}
\begin{lstlisting}[frame=single]


TFloat lambda(TFloat datos[], int n, TFloat beta){
       TFloat cero (0,datos[0].precision());
       TFloat uno (1,datos[0].precision());
       TFloat acum = R(datos,n,beta) - R(datos,n,cero);
       acum = beta * acum;
       acum = uno / acum;
       return acum;
}
\end{lstlisting}



\vspace{20pt}
\textbf{Ecuaci\'on para la estimaci\'on de $\sigma$}

\lstset{language=C, breaklines=true, basicstyle=\footnotesize}
\begin{lstlisting}[frame=single]

TFloat sigma (TFloat datos[], int n, TFloat beta, TFloat lambda){
    TFloat acum = M(datos,n,beta) / lambda;
    TFloat exp (1/(beta.dbl()),datos[0].precision());
    acum = TFloat (pow(acum.dbl(),exp.dbl()), acum.precision());
    return acum;
}
\end{lstlisting}

\vspace{20pt}
\textbf{M\'etodo de Newton}

\lstset{language=C, breaklines=true, basicstyle=\footnotesize}
\begin{lstlisting}[frame=single]
TFloat Newton(TFloat datos[], int n, TFloat x0, int iteraciones, double epsilon){
    int i = 0;
    TFloat xn = x0;
    TFloat x_anterior;
    do{
        x_anterior = xn;
        xn = xn - (funcion(datos, n, xn)/derivada(datos, n, xn));
        i++;
    }while(i<iteraciones && epsilon<fabs((xn-x_anterior).dbl()));

    cout << "Me paro: " << (i>=iteraciones? "Iteraciones" : "Epsilon") << ", en iteracion: " << i << endl;
    return xn;
}
\end{lstlisting}

\vspace{20pt}
\textbf{M\'etodo de la Secante}

\lstset{language=C, breaklines=true, basicstyle=\footnotesize}
\begin{lstlisting}[frame=single]
TFloat Secante(TFloat datos[], int n, TFloat x0, TFloat x1, int iteraciones, double epsilon){
    TFloat xn = x1;
    TFloat x_anterior = x0;
    TFloat acum;
    int i;
    for (i = 0 ; (i<iteraciones && epsilon<fabs((xn-x_anterior).dbl())) ; i++){
        acum = (xn - x_anterior) / (funcion(datos, n, xn) - funcion(datos, n, x_anterior));
        x_anterior = xn;
        xn = xn - (funcion(datos, n, xn) * acum);
    }

    cout << "Me paro: " << (i>=iteraciones? "Iteraciones" : "Epsilon") << ", en iteracion: " << i << endl;
    return xn;
}
\end{lstlisting}


